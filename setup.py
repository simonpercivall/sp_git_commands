#!/usr/bin/env python
import os
import re
from setuptools import setup, find_packages

readme = open('README.rst').read()
history = open('HISTORY.rst').read()


def read_version():
    with open(os.path.join('lib', 'sp_git_commands', '__init__.py')) as f:
        m = re.search(r'''__version__\s*=\s*['"]([^'"]*)['"]''', f.read())
        if m:
            return m.group(1)
        raise ValueError("couldn't find version")


install_requirements = [
    "click >= 6",
]


# NB: _don't_ add namespace_packages to setup(), it'll break
#     everything using imp.find_module
setup(
    name='sp_git_commands',
    version=read_version(),
    description='Collection of git commands I use.',
    long_description=readme + '\n\n' + history,
    author='Simon Percivall',
    author_email='percivall@gmail.com',
    url='https://bitbucket.org/simonpercivall/sp_git_commands/',
    packages=find_packages('lib'),
    package_dir={'': 'lib'},
    include_package_data=True,
    install_requires=install_requirements,
    entry_points={
        'console_scripts': [
            'git-up = sp_git_commands.cmds.up:cli',
            'git-cb = sp_git_commands.cmds.cb:cli',
            'git-copr = sp_git_commands.cmds.copr:cli',
            'git-rename-stash = sp_git_commands.cmds.rename_stash:cli',
            'git-rollback = sp_git_commands.cmds.rollback:cli',
            'git-safereset = sp_git_commands.cmds.safereset:cli',
            'git-whichjira = sp_git_commands.cmds.whichjira:cli',
        ],
    },
    license="BSD",
    zip_safe=False,
    keywords='sp_git_commands',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    test_suite='tests',
)
