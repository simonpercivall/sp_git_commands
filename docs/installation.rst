============
Installation
============

At the command line::

    $ easy_install sp_git_commands

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv sp_git_commands
    $ pip install sp_git_commands
