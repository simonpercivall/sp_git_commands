Welcome to My Git Commands's documentation!
===========================================

Contents:
=========

.. include:: ../README.rst
    :start-line: 16

.. toctree::
   :maxdepth: 2

   installation
   usage
   sp_git_commands
   contributing
   authors
   history

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

