===============
My Git Commands
===============

.. This is an example of how the readme could be decorated with badges.
    .. image:: https://badge.fury.io/py/sp_git_commands.png
        :target: http://badge.fury.io/py/sp_git_commands

    .. image:: https://travis-ci.org/<your github username>/sp_git_commands.png?branch=master
        :target: https://travis-ci.org/<your github username>/sp_git_commands

    .. image:: https://pypip.in/d/sp_git_commands/badge.png
        :target: https://crate.io/packages/sp_git_commands?version=latest


Collection of git commands I use.

* Free software: BSD license
* Documentation: http://sp_git_commands.rtfd.org.

Features
--------

* cb: Remove local branches where the remote branch is gone
* copr: Checkout branch for Github PR based on PR number
* rename-stash: Change a git stash message
* rollback: Roll back (part of) a specific file from the previous (or specified) commit
* safereset: Reset to upstream, while keeping working space changes
* up: A better (safer) pull, that also prunes remotes
* whichjira: Helps you find the merge commit for a commit (which includes Jira Id and Github PR number)
