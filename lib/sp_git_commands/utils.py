import shlex
import signal
import subprocess
import sys
from contextlib import contextmanager, suppress
from subprocess import run, CalledProcessError, PIPE, STDOUT

import click


__all__ = [
    'myrun', 'try_or_exit', 'autostash', 'exit_cpe',
    'PIPE', 'STDOUT',  # re-export
]


def myrun(*args, **kwargs) -> subprocess.CompletedProcess:
    # set some good defaults
    kwargs.setdefault('check', True)
    kwargs.setdefault('encoding', 'utf-8')
    kwargs.setdefault('universal_newlines', True)

    return run(*args, **kwargs)


def exit_cpe(e: CalledProcessError):
    # specialisation of CalledProcessError.__str__
    # show a nicer `cmd`
    cmd = str(e.cmd)
    with suppress(TypeError, ValueError):
        if not isinstance(e.cmd, str):
            cmd = ' '.join(shlex.quote(part) for part in e.cmd)
    if e.returncode and e.returncode < 0:
        try:
            sig = '%r' % signal.Signals(-e.returncode)
        except ValueError:
            sig = f'unknown signal {-e.returncode:d}'
        msg = f"Command '{cmd}' died with {sig}"
    else:
        msg = f"Command '{cmd}' return non-zero exit status {e.returncode}"

    click.echo(msg, err=True)
    click.echo(e.stderr, err=True)
    sys.exit(e.returncode)


def try_or_exit(*args, **kwargs):
    # otherwise, what's the point of this function?
    kwargs['check'] = True
    kwargs.setdefault('stderr', PIPE)

    try:
        return myrun(*args, **kwargs)
    except CalledProcessError as e:
        exit_cpe(e)


@contextmanager
def autostash():
    r = try_or_exit(['git', 'stash', 'push', '-m', 'temporary "git-up" stash'], stdout=subprocess.PIPE)
    try_unstash = 'No local changes to save' not in r.stdout

    try:
        yield
    finally:
        if try_unstash:
            cpe = myrun(['git', 'stash', 'pop'],
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        check=False)
            if cpe.returncode:
                click.echo(cpe.stderr)
