import re

import click

from ..utils import (
    PIPE,
    autostash,
    myrun,
    try_or_exit,
)


UPDATE_RE = re.compile(r'Updating ([\da-f]+\.\.[\da-f]+)$')


@click.command('git-up')
def cli():
    try_or_exit(['git', 'remote', 'update', '-p'])

    with autostash():
        r = try_or_exit(['git', 'merge', '--ff-only', '@{u}'], stdout=PIPE)
        print(r.stdout.strip())

    revision_range = None
    for line in r.stdout.splitlines():
        m = UPDATE_RE.match(line)
        if m:
            revision_range = m.group(1)
    if revision_range:
        myrun(['git', 'logdiff', revision_range], check=False)
    myrun(['git', 'cb'], check=False)
