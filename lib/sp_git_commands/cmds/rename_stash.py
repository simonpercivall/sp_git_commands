import re
from typing import NamedTuple

import click


from ..utils import (
    PIPE,
    myrun,
)


class Stash(NamedTuple):
    number: int
    name: str

    @property
    def rev_name(self):
        return f'stash@{{{self.number}}}'

    def get_commit_hash(self):
        proc = myrun(['git', 'rev-parse', self.rev_name],
                     stdout=PIPE)
        return proc.stdout.strip()


def get_stashes_by_number(s: str):
    return {int(match[0]): Stash(int(match[0]), match[1]) for match
            in re.findall(r'stash@{(\d+)\}: (.*)$', s, re.MULTILINE)}


@click.command('git rename-stash')
@click.argument('stash-number', type=int, required=False)
@click.option('-p', '--patch', 'show_patch', is_flag=True, help='Show patch before rename')
@click.option('-m', '--message', help='New name for stash')
def cli(stash_number, show_patch, message):
    proc = myrun([
        'git',
        'stash',
        'list'],
        stdout=PIPE)
    stashes = get_stashes_by_number(proc.stdout)

    def stash_number_value_proc(v):
        convert = click.types.FuncParamType(int)

        v = convert(v)
        if v not in stashes:
            raise click.BadParameter(f'Invalid stash number: not in [{", ".join(stashes)}]')

        return v

    if stash_number is None:
        print(proc.stdout)
        stash_number = click.prompt(click.style('Choose stash to rename', underline=True),
                                    type=int, value_proc=stash_number_value_proc)

    stash = stashes[stash_number]
    if message is None:
        if show_patch:
            myrun(['git', 'stash', 'show', '-p', stash.rev_name], check=False)
        message = click.prompt(click.style('Enter new name', underline=True))

    # we're all good, let's do the rename
    commit_hash = stash.get_commit_hash()
    myrun(['git', 'stash', 'drop', stash.rev_name], check=False)
    myrun([
        'git', 'stash', 'store',
        '-m', message,
        commit_hash,
    ], check=False)
