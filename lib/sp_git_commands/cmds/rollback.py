from typing import List

import click

from ..utils import myrun


@click.command('git-rollback', help='''\
Rollback part of the latest commit, while keeping the changes in the
worktree.

This is a thin wrapper around `reset <paths>...`, which does exactly
the same thing.

A path is required to make the intent more explicit, i.e. the purpose
is to rollback only part of a commit, not the whole.
''')
@click.option('-p', '--patch', is_flag=True)
@click.option('-c', '--commit', is_flag=True)
@click.argument('paths', metavar='PATH[, ...]', type=click.Path(exists=True),
                nargs=-1, required=True)
def cli(
    *,
    commit: bool,
    patch: bool,
    paths: List[str],
):
    cmdline = [
        'git',
        'reset',
        'HEAD^',
    ]
    if patch:
        cmdline += ['-p']
    cmdline.extend(paths)

    myrun(cmdline)
    if commit:
        myrun([
            'git',
            'commit',
            '--amend',
            '--no-edit',
        ])
