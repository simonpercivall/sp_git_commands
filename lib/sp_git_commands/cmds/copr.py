import re

import click

from ..utils import (
    PIPE,
    try_or_exit,
)


@click.command('git-copr')
@click.argument('pr', type=int)
def cli(pr: int):
    try_or_exit([
        'git', 'fetch', 'origin', f'pull/{pr}/head'],
        stdout=PIPE)
    fetch_head = try_or_exit([
        'git', 'rev-parse', 'FETCH_HEAD',
    ], stdout=PIPE).stdout.strip()
    try_or_exit([
        'git', 'remote', 'update', '-p'],
        stdout=PIPE)
    remote_branches = try_or_exit([
        'git', 'for-each-ref', '--format', '%(objectname):%(refname)', 'refs/remotes/origin',
    ], stdout=PIPE).stdout.splitlines()

    for remote_branch in remote_branches:
        commit, _, ref_branch_name = remote_branch.partition(':')
        # we get refs/remotes/origin/, strip that away
        remote_branch_name = re.sub(r'^refs/remotes/origin/', '', ref_branch_name)
        if commit == fetch_head:
            try_or_exit([
                'git', 'checkout', remote_branch_name])
            try_or_exit([
                'git', 'merge', '--ff-only', '@{u}'])
            break
    else:
        click.Abort(f"Found no remote branch for PR {pr}")
