import click

from ..utils import try_or_exit


@click.command('git-whichjira')
@click.argument('from_commit')
def cli(from_commit):
    return try_or_exit([
        'git',
        'log',
        f'{from_commit}..master',
        '--ancestry-path',
        '--merges',
    ])
