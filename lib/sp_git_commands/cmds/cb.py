import re
from subprocess import CalledProcessError

import click

from ..utils import (
    PIPE,
    exit_cpe,
    myrun,
    try_or_exit,
)


@click.command('git-cb')
def cli():
    cp = try_or_exit([
        "git",
        "for-each-ref",
        "--format=%(upstream:track,nobracket)::%(refname:short)",
        "refs/heads/"],
        stdout=PIPE)
    for ref in cp.stdout.splitlines():
        track, _, branch = ref.partition('::')
        if track == 'gone':
            try:
                myrun([
                    "git",
                    "branch",
                    "-d", branch,
                ], stderr=PIPE)
            except CalledProcessError as e:
                unmerged_branch = format_nonmerged_branch(e.stderr)
                if not unmerged_branch:
                    exit_cpe(e)
                else:
                    click.echo(f"Could not automatically delete unmerged "
                               f"branch '{unmerged_branch}'.", err=True)
                    myrun(['git', 'branch', '-vv'])
                    should_delete_branch = click.prompt(
                        f"Do you want to remove '{branch}'?",
                        default=False)
                    if should_delete_branch:
                        try_or_exit([
                            'git',
                            'branch',
                            '-D', branch
                        ])


def format_nonmerged_branch(text: str) -> str:
    m = re.search("""The branch ['"]([\S]+?)['"] is not fully merged.""",
                  text, re.MULTILINE)
    if not m:
        return ''
    return m.group(1)
