import click

from ..utils import (
    PIPE,
    autostash,
    myrun,
    try_or_exit,
)


@click.command('git-safereset',
               help="Reset local branch to remote, while keeping working space changes")
def cli():
    """
    * Find upstream branch
        * if no upstream branch: exit
    * autostash
        * reset hard to upstream branch
    """
    try_or_exit([
        'git',
        'rev-parse',
        '--abbrev-ref',
        '--symbolic-full-name',
        '@{u}',
    ], stdout=PIPE)
    with autostash():
        myrun(['git', 'reset', '--hard', '@{u}'])
